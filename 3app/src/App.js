import './App.css';
import React, {useRef} from "react";
import cancion from "./assets/video.mp4"

function App() {
  
  const videoREF =useRef(null);
  
  const videoPlay = () =>{
    videoREF.current.play();
  };

  const videoPause = () =>{
    videoREF.current.pause();
  };
  return(
    <di>
      
      <video ref={videoREF} controls>
        <source src={cancion} type="video/mp4"></source>
      </video>
      
      <div>
        <button onClick={videoPlay}>Play</button>
        <button onClick={videoPause}>Pause</button>

      </div>
    </di>
  )
}

export default App;
