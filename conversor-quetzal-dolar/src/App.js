import './App.css';

import {useRef, useState, useEffect} from "react"
function App() {

  const [valorCambio, setValorCambio] = useState(null)

  const quetzalRef=useRef();
  const resultadoRef=useRef();

  useEffect(()=>{
    const callApiChange=async()=>{
      try{
        const resp = await fetch("https://v6.exchangerate-api.com/v6/eaf29b2fd08af6b443da11d5/latest/GTQ");

        const datos = await resp.json();
        console.log(datos);
        setValorCambio(datos.conversion_rates.USD);
      }catch(error){
        console.log("Error al acceder a la API: ", error);
      }
    };

    callApiChange();
  }, []);

  const calcular=()=>{
    const quetzalValor = parseFloat(quetzalRef.current.value);
    const dolares=quetzalValor*valorCambio;
    resultadoRef.current.innerHTML="$ "+dolares.toFixed(2);
  }
  return <div>
    <h1 className='title'>Conversor de Quetzal a Dolar</h1>
    <input className='centrarElement' type='text' ref={quetzalRef}></input>
    <button className='centrarElement' onClick={calcular}>Convertir</button>

    <div className='centrarElement result' ref={resultadoRef}></div>

  </div>
}

export default App;
