
import './App.css';
import React, {useState} from "react";

function App() {
  
  // Definir el estado para almacenar el resultado de la suma
  const [resultado, setResultado]=useState(null);
  const elemento = <h1 className="centrar-titulo">Hola mundo</h1>
  // const element2 = <h2>{suma(2,2)}{/*comentarios en JSX}*/}</h2>;

  const botonPulsado=()=>{
    const result=suma(4,4);
    
    setResultado(result)
  }

  return <div>
    
      <div>{elemento}</div>
      <button onClick={botonPulsado} style={{marginTop: "10px", marginLeft: "10px"}}>Pulsame</button>
      <div> { resultado!=null && <h2>El resultado es: {resultado}</h2>} </div>
  </div>
}

// const botonPulsado=()=>{
//   alert(suma(3,3));
// }

function suma(a, b){
  return a+b;
}

export default App;

