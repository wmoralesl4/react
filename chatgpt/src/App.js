import React, { useState, useRef } from 'react';
import cancion from "./assets/video.mp4"

function App() {
  const [isPlaying, setIsPlaying] = useState(false);
  const videoRef = useRef(null);

  const handlePlayPause = () => {
    if (isPlaying) {
      videoRef.current.pause();
    } else {
      videoRef.current.play();
    }
    setIsPlaying(!isPlaying);
  };

  return (
    <div className="container mt-5">
      <div className="row">
        <div className="col-12 text-center">
          <h1>Video Player</h1>
          <video ref={videoRef} width="600" className="mt-3" controls>
            <source src={cancion} type="video/mp4" />
            Your browser does not support the video tag.
          </video>
          <div className="mt-3">
            <button className="btn btn-primary" onClick={handlePlayPause}>
              {isPlaying ? 'Pause' : 'Play'}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
